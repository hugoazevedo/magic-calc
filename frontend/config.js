const api_config = function(){
  if (process.env.NODE_ENV == "development"){
    return 'http://guu.ocp.ibm.edu:8383'
  } else if (process.env.NODE_ENV == "production") {
    return 'http://guu.ocp.ibm.edu:8383'
  }
}();

const GCP_KEY = null;
const AWS_KEY = null;
const AWS_SECRET = null;

export {
  api_config, AWS_KEY, GCP_KEY, AWS_SECRET
}
